# โปรเจกต์ร้านขายนาฬิกาออนไลน์ใช้ React.js

## การติดตั้งโปรเจกต์

### ติดตั้งแพคเกจที่จำเป็นทั้งหมด

### To intsall all the required dependecies

```
npm install
```

### เริ่มโปรเจกต์ 
## Start the project

```
npm start
```

เปิดลิงก์ http://localhost:3000 ในเบราว์เซอร์เพื่อดูโปรเจกต์

### Test the application

```
npm test
```

### รูปตัวอย่างโปรเจกต์

[![1.png](https://i.postimg.cc/9QCxBF6T/1.png)](https://postimg.cc/Xp1cj377)
[![2.png](https://i.postimg.cc/TwLQPyb8/2.png)](https://postimg.cc/gXp3S244)
[![3.png](https://i.postimg.cc/9M6bwWPD/3.png)](https://postimg.cc/bDRkKfBP)
[![4.png](https://i.postimg.cc/Y0Q31J8b/4.png)](https://postimg.cc/XB7dW1RC)
[![5.png](https://i.postimg.cc/P5X4wVDQ/5.png)](https://postimg.cc/7fR0vVm5)
[![6.png](https://i.postimg.cc/W4tw9DXD/6.png)](https://postimg.cc/QBG7CMLs)