# ใช้ Node.js เวอร์ชัน 18 บน Alpine Linux เป็น base image
FROM node:18-alpine

# ตั้งค่า working directory เป็น /app
WORKDIR /app

# คัดลอกไฟล์ package.json, package-lock.json และ postcss.config.js ไปยัง working directory
COPY package*.json ./
COPY postcss.config.js ./

# ติดตั้ง dependencies ของโปรเจค
RUN npm i

# คัดลอกโค้ดทั้งหมดไปยัง working directory
COPY . .

# ติดตั้ง Tailwind CSS
RUN npm install -D tailwindcss@latest postcss@latest autoprefixer@latest

# build โปรเจค Next.js
RUN npm run build

# expose port ที่ใช้โดย Next.js
EXPOSE 3000

# start server สำหรับ production
CMD ["npm", "start"]