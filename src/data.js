export const storeProducts = [
  {
    id: 1,
    title: "NAVY SEAL 3500 SERIES รุ่น XS.3501.SPARTAN",
    img: "/img/product-1.png",
    price: 250,
    company: "LUNI NOX",
    info:
      "สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม      รับประกันเฉพาะเครื่องและถ่าน 1 ปี      ในระยะประกันห้ามเปิดเครื่องที่อื่น      มีบริการปรับสายฟรี ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 2,
    title: "ASIA LITE 0320.AS SERIES รุ่น XS.0323.AS",
    img: "/img/product-2.png",
    price: 160,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 3,
    title: "LEATHERBACK SEA TURTLE GIANT 0320 SERIES รุ่น XS.0321.BO.L",
    img: "/img/product-3.png",
    price: 300,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 4,
    title: " NAVY SEAL STEEL COLORMARK CHRONOGRAPH 3180 SERIES รุ่น XS.3181.F",
    img: "/img/product-4.png",
    price: 1500,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 5,
    title: "NAVY SEAL 3500 SERIES รุ่น XS.3501.CARN.N",
    img: "/img/product-5.png",
    price: 24,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 6,
    title: "AUTOMATIC LIMITED EDITION SPORT TIMER 0920 SERIES รุ่น XS.0927",
    img: "/img/product-6.png",
    price: 4500,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 7,
    title: "P-38 LIGHTNING 9520 SERIES รุ่น XA.9527",
    img: "/img/product-7.png",
    price: 700,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  },
  {
    id: 8,
    title: "PACIFIC DIVER 3120 SERIES รุ่น XS.3121.BO",
    img: "/img/product-8.png",
    price: 820,
    company: "LUNI NOX",
    info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
    inCart: false,
    count: 0,
    total: 0
  }
];

export const detailProduct = {
  id: 1,
  title: "",
  img: "/img/product-1.png",
  price: 10,
  company: "LUNI NOX",
  info:"โทนสีของผลิตภัณฑ์ในรายการอาจแตกต่างจากผลิตภัณฑ์จริงในบางกรณี    สินค้าใหม่ พร้อมกล่อง คู่มือ ป้าย ใบรับประกัน 1 ปีเต็ม    รับประกันเฉพาะเครื่องและถ่าน 1 ปี    ในระยะประกันห้ามเปิดเครื่องที่อื่น    มีบริการปรับสายฟรี (ลูกค้าแจ้งขนาดข้อมือก่อนทำการจัดส่ง)",
  inCart: false,
  count: 0,
  total: 0
};
