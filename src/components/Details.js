import React, { Component } from "react";
import { ProductConsumer } from "../context";
import { Link } from "react-router-dom";
import { ButtonContainer } from "./Button";
import { ThemeConsumer } from "./context/ThemeContexts";

export default class Details extends Component {
  render() {
    return (
      <ThemeConsumer>
        {({ theme }) => (
          <ProductConsumer>
            {(value) => {
              const { id, company, img, info, price, title, inCart } =
                value.detailProduct;
              return (
                <div className={theme ? "bg-slate-900 text-white" : ""}>
                  <div className="container py-5">
                    <div className="row">
                      <div className="col-10 mx-auto col-md-6 my-3">
                        <img src={img} className="img-fluid" alt="product" />
                      </div>
                      <div className="col-10 mx-auto col-md-6 my-3">
                        <h1 className="display-4 font-weight-bold mb-4">
                          {title}
                        </h1>
                        <p className="lead">
                          <strong>Model:</strong> {title}
                        </p>
                        <p className="lead">
                          <strong>Made by:</strong> {company}
                        </p>
                        <p
                          className={theme ? "text-primary h3" : "text-blue h3"}
                        >
                          <strong>Price: ${price}</strong>
                        </p>
                        <p className="lead font-weight-bold">
                          Product Description:
                        </p>
                        <p>{info}</p>
                        <div className="d-flex justify-content-between">
                          <Link to="/">
                            <ButtonContainer>Back to Products</ButtonContainer>
                          </Link>
                          <ButtonContainer
                            cart
                            disabled={inCart ? true : false}
                            onClick={() => {
                              value.addToCart(id);
                              value.openModal(id);
                            }}
                          >
                            {inCart ? "In Cart" : "Add to Cart"}
                          </ButtonContainer>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="container">
                    <div className="row my-5">
                      <div className="col">
                        <h2 className="text-center mb-4">Related Products</h2>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">
                        <h2 className="text-center mb-4">Product Reviews</h2>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }}
          </ProductConsumer>
        )}
      </ThemeConsumer>
    );
  }
}
