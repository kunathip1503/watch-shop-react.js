import React, { Component } from "react";
import Title from "../Title";
import CartColumns from "./CartColumns";
import EmptyCart from "./EmptyCart";
import { ProductConsumer } from "../../context";
import CartList from "./CartList";
import CartTotals from "./CartTotals";

export default class Store extends Component {
  render() {
    return (
      <section className="py-5">
        <div className="container">
          <ProductConsumer>
            {(value) => {
              const { cart } = value;
              if (cart.length > 0) {
                return (
                  <React.Fragment>
                    <Title name="your" title="cart" />
                    <div className="row">
                      <div className="col-10 mx-auto">
                        <CartColumns />
                        <CartList value={value} />
                        <div className="mt-5">
                          <CartTotals
                            value={value}
                            history={this.props.history}
                          />
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                );
              } else {
                return (
                  <div className="text-center mt-5">
                    <EmptyCart />
                  </div>
                );
              }
            }}
          </ProductConsumer>
        </div>
      </section>
    );
  }
}
