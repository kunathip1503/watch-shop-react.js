import React, { Component } from "react";
import { Link } from "react-router-dom";
import logo from "../logo.svg";
import styled from "styled-components";
import { ButtonContainer } from "./Button";
import { ThemeContext } from "./context/ThemeContexts";
import { FaRegMoon, FaShoppingCart } from "react-icons/fa";
import { GoSun } from "react-icons/go";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import { ProductConsumer } from "../context";
import { NavLink } from "react-router-dom";

class Navbar extends Component {
  static contextType = ThemeContext;

  constructor(props) {
    super(props);
    this.state = {
      isMobile: window.innerWidth <= 768,
      menuOpen: false,
    };

    this.handleResize = this.handleResize.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  handleResize() {
    this.setState({
      isMobile: window.innerWidth <= 768,
    });
  }

  handleMenu() {
    this.setState((prevState) => ({ menuOpen: !prevState.menuOpen }));
  }

  render() {
    const { theme, toggleTheme } = this.context;
    const { isMobile, menuOpen } = this.state;

    return (
      <NavWrapper
        className={`navbar nav-bar-expand-sm ${
          theme ? "bg-slate-800" : "bg-white"
        } px-sm-5`}
      >
        <div className="container">
          <Link to="/" className="navbar-brand">
            <img src={logo} alt="store" className="logo" />
          </Link>
          {isMobile ? (
            <>
              <div
                className={`menu-icon ${theme ? "text-white" : "text-dark"}`}
                onClick={this.handleMenu}
              >
                {menuOpen ? <AiOutlineClose /> : <AiOutlineMenu />}
              </div>
              {menuOpen && (
                <div
                  className={`mobile-menu ${
                    theme ? "bg-slate-800" : "bg-white"
                  }`}
                >
                  <NavLink
                    to="/"
                    className={({ isActive }) =>
                      isActive ? "active-link" : "nav-link"
                    }
                    onClick={this.handleMenu}
                  >
                    Products
                  </NavLink>
                  <ProductConsumer>
                    {(value) => (
                      <div className="search-bar">
                        <input
                          placeholder="Search for products"
                          onChange={(e) => {
                            value.filterProducts(e.target.value);
                          }}
                        />
                      </div>
                    )}
                  </ProductConsumer>
                  <div className="theme-toggle" onClick={toggleTheme}>
                    {theme ? <FaRegMoon /> : <GoSun />}
                    <span>{theme ? "Dark Mode" : "Light Mode"}</span>
                  </div>
                  <Link
                    to="/cart"
                    className="cart-link"
                    onClick={this.handleMenu}
                  >
                    <ButtonContainer>
                      <FaShoppingCart />
                      My Cart
                    </ButtonContainer>
                  </Link>
                </div>
              )}
            </>
          ) : (
            <>
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <NavLink
                    to="/"
                    className={({ isActive }) =>
                      isActive ? "active-link" : "nav-link"
                    }
                  >
                    Products
                  </NavLink>
                </li>
              </ul>
              <ProductConsumer>
                {(value) => (
                  <div className="search-bar">
                    <input
                      placeholder="Search for products"
                      onChange={(e) => {
                        value.filterProducts(e.target.value);
                      }}
                    />
                  </div>
                )}
              </ProductConsumer>
              <div className="navbar-nav ml-auto d-flex align-items-center">
                <div className="theme-toggle mr-3" onClick={toggleTheme}>
                  {theme ? <FaRegMoon /> : <GoSun />}
                </div>
                <Link to="/cart" className="cart-link">
                  <ButtonContainer>
                    <FaShoppingCart /> My Cart
                  </ButtonContainer>
                </Link>
              </div>
            </>
          )}
        </div>
      </NavWrapper>
    );
  }
}

const NavWrapper = styled.nav`
  .navbar-brand {
    .logo {
      height: 40px;
    }
  }

  .nav-link {
    color: var(--mainDark);
    font-size: 1.2rem;
    text-transform: uppercase;
    padding: 0.5rem 1rem;
    transition: color 0.3s;

    &:hover {
      color: var(--mainBlue);
    }
  }

  .active-link {
    color: var(--mainBlue);
    font-weight: bold;
  }

  .search-bar {
    input {
      padding: 0.5rem;
      border: none;
      border-radius: 4px;
      margin-right: 1rem;
    }
  }

  .theme-toggle {
    display: flex;
    align-items: center;
    cursor: pointer;
    margin-right: 1rem;

    svg {
      font-size: 1.5rem;
      margin-right: 0.5rem;
    }

    span {
      font-size: 1rem;
    }
  }

  .cart-link {
    margin-left: 1rem;
  }

  .menu-icon {
    font-size: 1.5rem;
    cursor: pointer;
  }

  .mobile-menu {
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    padding: 1rem;
    z-index: 100;

    .nav-link,
    .active-link {
      display: block;
      padding: 0.5rem 0;
    }

    .search-bar {
      margin-bottom: 1rem;
    }

    .theme-toggle {
      margin-bottom: 1rem;
    }
  }
`;

export default Navbar;
